package views

import (
	"image/color"

	"gitlab.com/k-terashima/casino-21/models"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

// plotXYs データをXYに代入
func PlotXYs(cards []models.Card) plotter.XYs {
	var p = make(plotter.XYs, len(cards))
	for i, v := range cards {
		p[i].X = float64(i)
		p[i].Y = float64(v.Number)
	}

	return p
}

// プロット描画
// 乱数検証のため
func PlotData(xys plotter.XYs) {
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = "配布カード分散チェック"
	p.X.Label.Text = "配列"
	p.Y.Label.Text = "デッキ"

	s, err := plotter.NewScatter(xys)
	if err != nil {
		panic(err)
	}

	// 描画pointの大きさ・色
	s.Radius = vg.Length(2)
	s.Color = color.RGBA{R: 0, G: 0, B: 255, A: 255}
	p.Add(s)
	// Plot描画
	filename := "../_static/img/plot.png"
	if err = p.Save(10*vg.Inch, 6*vg.Inch, filename); err != nil {
		panic(err)
	}
}
